const express = require("express");
const { getAllTodos } = require ("../controllers/todo.controllers.js");

const router = express.router();

router.get("/", getAllTodos);